#!/usr/bin/env python3

'''
                                                          dP                  dP MM'""""'YMM M""MMMMM""M
                                                          88                  88 M' .mmm. `M M  MMMMM  M 
.d8888b. dP    dP .d8888b. 88d8b.d8b. .d8888b. 88d888b. d8888P .d8888b. .d888b88 M  MMMMMooM M  MMMMP  M 
88'  `88 88    88 88'  `88 88'`88'`88 88ooood8 88'  `88   88   88ooood8 88'  `88 M  MMMMMMMM M  MMMM' .M 
88.  .88 88.  .88 88.  .88 88  88  88 88.  ... 88    88   88   88.  ... 88.  .88 M. `MMM' .M M  MMP' .MM 
`88888P8 `88888P' `8888P88 dP  dP  dP `88888P' dP    dP   dP   `88888P' `88888P8 MM.     .dM M     .dMMM 
                       .88                                                       MMMMMMMMMMM MMMMMMMMMMM 
                   d8888P                                                                                
'''

from twisted.internet import reactor, endpoints

# for now this is a mere hello-world, checking whether importing works...

from augmented_cv.utils import dep_check
from augmented_cv.proto import EchoFactory, ImreadFactory

dep_check()


endpoints.serverFromString(reactor, "tcp:1234").listen(EchoFactory())
endpoints.serverFromString(reactor, "tcp:5555").listen(ImreadFactory())
reactor.run()
