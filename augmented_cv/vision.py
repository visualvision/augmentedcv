#!/usr/bin/env python3
'''
`augmented_cv.vision` -- Module for holding the OpenCV facing code
'''

import cv2
import numpy as np



# TODO: implement
def draw_rectangle():
    pass


def shape_reader(fname):
    '''
    return an Image's shape information (height, width, channels)
    '''
    i = cv2.imread(fname)
    return i.shape



def detect_edges(fname):
    '''
    peform Canny Edge Detection on given local image file
    '''


    #########################
    ##### Actual CV code....
    #########################
    i = cv2.imread(fname)

    # doing Canny Edge detection
    # read https://www.pyimagesearch.com/2021/05/12/opencv-edge-detection-cv2-canny/
    # for more information

    gray = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)

    # create wide, mid and tight edges
    wide = cv2.Canny(blurred, 10, 200)
    mid = cv2.Canny(blurred, 30, 150)
    tight = cv2.Canny(blurred, 240, 250)

    fwide = "{}.edge.wide.jpg".format(fname)
    fmid = "{}.edge.mid.jpg".format(fname)
    ftight = "{}.edge.tight.jpg".format(fname)


    # NOTE: That this in-progress status is no longer available client-side but only in the daemon's stdout
    # just save them next to the original file
    print("[*] saving {}\n".format(fwide).encode('utf-8'))
    cv2.imwrite(fwide, wide)
    print("[*] saving {}\n".format(fmid).encode('utf-8'))
    cv2.imwrite(fmid, mid)
    print("[*] saving {}\n".format(ftight).encode('utf-8'))
    cv2.imwrite(ftight, tight)


def conv_matrix_3x3(fname):
    '''
    return an image's convolution matrix
    '''
    i = cv2.imread(fname)

    kernel = np.array([[1, 1, 1],
                       [1, 1, 1],
                       [1, 1, 1]])


    #cv2.filter2D()


def conv_lowpass(fname):
    '''
    Apply convolutional lowpass filtering on the image using Numpy Array as Kernel
    '''
    i = cv2.imread(fname)


    kernel = np.array([
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1]
    ])

    kernel = kernel/sum(kernel)

    lowpass = cv2.filter2D(i, -1, kernel)
    f_lowpass = "{}.lowpass.jpg".format(fname)

    print("[*] saving {}\n".format(f_lowpass))
    cv2.imwrite(f_lowpass, lowpass)

def conv_highpass(fname):
    '''
    Apply highpass filtering by applying custom kernel
    '''
    i = cv2.imread(fname)

    kernel = np.array([[0.0, -1.0, 0.0],
                       [-1.0, 4.0, -1.0],
                       [0.0, -1.0, 0.0]])

    kernel = kernel/np.sum(kernel) if np.sum(kernel) != 0 else 1

    highpass = cv2.filter2D(i, -1, kernel)
    f_highpass = "{}.highpass.jpg".format(fname)

    print("[*] saving {}\n".format(f_highpass))
    cv2.imwrite(f_highpass, highpass)
