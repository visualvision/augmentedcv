#!/usr/bin/env python3

import os

def isfile(f):
    '''

    Parameters
    ----------

    f: str

    Yields
    ------

    bool
        whether there is a file named like the provided string

    '''
    return os.path.isfile(f)


def dep_check():
    '''
    validates that all required dependencies are installed
    '''

    i = 'cv2'
    try:
        import cv2
        print("[+] successfully imported {} module!".format(i))
    except:
        print("[!] failed to import {} module!".format(i))

    i = 'twisted'
    try:
        import twisted
        print("[+] successfully imported {} module!".format(i))
    except:
        print("[!] failed to import {} module!".format(i))

    '''
    # Old version => doesn't work because module has to be imported already
    # `import eval(i) throws syntax error`

    for i in ['cv2', 'twisted']:
        try:
            import eval(i)
            print("[+] successfully imported {} module!".format(i))
        except:
            print("[!] failed to import {} module!".format(i))
    '''
