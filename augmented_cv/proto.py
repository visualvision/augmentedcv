#!/usr/bin/env python3
'''
augmented_cv.proto -- holding Protocols and Related Factories for Twisted Service instantation
'''
from twisted.internet import protocol

from .utils import isfile
from .vision import detect_edges, shape_reader
from .vision import conv_highpass, conv_lowpass, conv_matrix_3x3

import os


# Example code, see  twistedmatrix.com.
class Echo(protocol.Protocol):
    def dataReceived(self, data):
        self.transport.write(data)

class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Echo()


class Imread(protocol.Protocol):
    '''
    Service that reads in (local) images on demand controlled via tcp port
    '''
    hist = []

    def __init__(self):
        self.hist = []

    def _help(self):
        s = "=== augmentedCV ===\n"
        s += "available commands:\n"
        s += "h         -- print this help message\n"
        s += "r <FILE>  -- read a file\n"
        s += "e <FILE>  -- perform edge detection\n"
        s += "c         -- convolutional mode\n"
        s += "l         -- list all images\n"
        s += "q         -- exit\n"
        return s

    def _help_c(self):
        s = ">>> Convolutional Mode <<<\n"
        s += "available commands:\n"
        s += "cm3         -- return a convolutional matrix of 3x3\n"
        #s += "cm5         -- return a convolutional matrix of 5x5\n"
        s += "cl <IMG>    -- apply lowpass filter to IMG\n"
        s += "ch <IMG>    -- apply highpass filter to IMG\n"
        return s

    def dataReceived(self, data):
        bdata = data
        data = bdata.decode('utf-8')
        #print("[DBG] data ({}): {}".format(type(data), data))
        #print("[DBG] bdata ({}): {}".format(type(bdata), bdata))

        ######################################################
        # QUIT / EXIT
        if data == 'q\r\n' or data == 'exit\r\n':
            self.transport.write(b"[*] Exiting...\n")
            self.transport.loseConnection()
        ######################################################
        # HELP
        elif data.startswith('h ') or data == 'h':
            self.transport.write(self._help().encode('utf-8'))
        ######################################################
        # READ IMG => get pixel extents
        elif data.startswith('r '):
            _, fname = data.strip('\r\n').split(' ')
            if not fname or fname == '':
                self.transport.write(b"[!] no image selected.\n")
            elif not isfile(fname):
                self.transport.write("[!] {} does not exist.\n".format(fname).encode('utf-8'))
            else:
                self.transport.write("[*] Processing image.... {}\n".format(fname).encode('utf-8'))
                (h, w, c) = shape_reader(fname) # use `augmented_cv.vision.shape_reader(fname)`
                self.transport.write("{} is {}x{} and has {} channels\n".format(fname, w, h, c).encode('utf-8'))
                self.hist.append({ "fname": fname, "height": h, "width": w, "channels": c })
        ######################################################
        # EDGE DETECTOR MODE
        elif data.startswith('e '):
            _, fname = data.strip('\r\n').split(' ')
            if not fname or fname == '':
                self.transport.write(b"[!] no image selected.\n")
            elif not isfile(fname):
                self.transport.write("[!] {} does not exist.\n".format(fname).encode('utf-8'))
            else:
                self.transport.write("[*] Detecting edges on.... {}\n".format(fname).encode('utf-8'))
                # NOTE: this reduces complexity, but it also removes the ability to inform the client of ongoing operations
                detect_edges(fname)  # `augmented_cv.vision.detect_edges(fname)`
                self.transport.write("[=] Done.\n".encode('utf-8'))
        ######################################################
        # CONVOLUTIONAL OPERATIONS
        elif data.startswith('c'):
            if data.startswith("cm3 "):
                _, fname = data.strip('\r\n').split(' ')
                conv_matrix_3x3(fname)
            elif data.startswith("cl "):
                _, fname = data.strip('\r\n').split(' ')
                conv_lowpass(fname)
            elif data.startswith("ch "):
                _, fname = data.strip('\r\n').split(' ')
                conv_highpass(fname)
            else:
                self.transport.write(self._help_c().encode('utf-8'))

        ######################################################
        # LIST HISTORY
        elif data == 'l\r\n':
            self.transport.write(b'[*] l selected.\n')
            for f in self.hist:
                self.transport.write('{} ({}x{}, {} channels)\n'.format(f['fname'], f['height'], f['width'], f['channels']).encode('utf-8'))
                #self.transport.write('{} ({}x{}, {} channels)'.format(f.fname, f.height, f.width, f.channels).encode('utf-8'))
        ######################################################
        # ELSE PRINT HELP
        else:
            self.transport.write(self._help().encode('utf-8'))
            # ... do acutal CV stuff

class ImreadFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Imread()
