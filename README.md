# augmentedCV

computer + reality = <3



A project leveraging openCVs capabilities to augment the Interaction with Microcontrollers and Boards



![augmentedCV](augmentedCV_LOGO.png)






## Getting started

run the following commands to run augmentedCV

```
git clone https://gitlab.com/visualvision/augmentedCV

cd augmentedCV

pipenv shell
pipenv install
# OR
python3 -m venv env
source env/bin/activate
pip3 install -r req.txt

python app.py

```

this will start a twisted web service

in another shell window, do the following:

```
telnet localhost 5555

h

r boards/pico.jpg
r boards/uno.jpg
l

e boards/nano.jpg

q
```

to connect to the imread-service port and start interacting with augmentedCV
