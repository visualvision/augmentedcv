:css: style.css

.. title:: augmentedCV

----

:data-x: r3440

.. image:: images/logo_big_blur.png
   :width: 600px

augmentedCV
==================

Computer + Reality = <3

Marvin Nikolas Evers, Benedikt Filzen


.. note::

  * Moinmoin diesdas
  * Thema Augmented Reality per OpenCV

----

.. image:: images/kabelsalat.png
   :target: https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/IoT-Werkstatt/octopus/DIY-Octopus2.jpg


Idee
------

* Laien an die Schaltkreise
* Portbelegung in die Reality augmentieren
* Hilfe bei Suche von Fehlerquellen
* Arbeitsumgebung mit Kontext anreichern


----

.. image:: images/logo_python.png
   :target: https://www.marinedatascience.co/img/software/logo_python.png
   :width: 200px
.. image:: images/OpenCV_Logo.svg
   :target: https://commons.wikimedia.org/wiki/File:OpenCV_Logo_with_text_svg_version.svg
   :width: 200px
.. image:: images/40070-webcam-tool-black-circular-shape.svg
   :target: http://freevector.co/vector-icons/tools-and-utensils/webcam-tool-black-circular-shape.html
   :width: 200px

Umsetzung
---------


* Python
* OpenCV

  * markerless

* Webcam

Fokus auf Implementierung von Boardoverlays - reines proof of concept


.. note::

  * kein hübsches UI Zeugs - reines mapping
  * keine fancy Brillensache - später mehr dazu
  * Pinlayoutbild auf Singleboardcomputer mappen
  * wenn wir gut sind noch 3D-Kram

----

:data-x: r0
:data-y: r1440

.. image:: images/pico-layout.png
   :target: https://cdn-blog.adafruit.com/uploads/2021/01/raspberry_pi_pico_pinout-1024x745-1.png

Soll heißen ?
-------------

* Pins zählen macht keinen Spaß
* augmented ist es viel cooler

.. note::

  * dauert zu lang
  * ist fehleranfällig
  * daher frustrierend
  * ein "schwebendes hologramm" zeigt sofort an
  * also diese pinoutdiagramm transparent über ein echtes pcb
  * interaktive tutorials auch möglich

----

:data-x: r3440
:data-y: r0

.. image:: images/ar-brille.webp
   :target: https://content.instructables.com/ORIG/FZ2/S71F/KIOKGR43/FZ2S71FKIOKGR43.png?auto=webp&frame=1&width=1024&fit=bounds&md=66eb9c28f070e44259a6e50335d34891

Hardware?
---------------

Während Projekt: **Nein**

* eine handvoll FOSS Hardware existiert
* günstig in der Anschaffung
* Bastelansporn

.. note::

  * Beispiel für ESP32 basierte AR-Brille
  * auch nicht mehr als proof of concept

----

.. image:: images/logo_big_white.png

Danke!
------

.. note::

  * weitere Fragen?
  * Links zu Bildern als clickevents auf Bildern
