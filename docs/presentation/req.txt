docutils==0.17.1
hovercraft==2.7
lxml==4.6.3
Pygments==2.9.0
svg.path==4.1
watchdog==2.1.1
